﻿#include <iostream>
#include <string>

int main()
{    
    std::string text = "GameBox";

    std::cout << "Text: " << text << std::endl;
    std::cout << "Text size: " << text.size() << std::endl;
    std::cout << "First symbol: " << text[0] << std::endl;
    std::cout << "Last symbol: " << text[text.size() - 1] << std::endl;

    return 0;
}